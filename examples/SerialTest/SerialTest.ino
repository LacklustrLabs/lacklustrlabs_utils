#include "lacklustrlabs_utils.h"

#if defined(ARDUINO_ARCH_STM32F1)
#define SOUT Serial1
#else
#define SOUT Serial
#endif

void setup() {
  SOUT.begin(115200);
}

void loop() {
  delay(1000);
  uint32_t large = 0xFF001113;
  SOUT.print("large  = 0b");
  lacklustrlabs::Utils::printBinary(SOUT, large);
  SOUT.print(" == ");
  SOUT.print(lacklustrlabs::Utils::numeric2HexString(large));
  SOUT.println();

  uint16_t medium = 0x1113;
  SOUT.print("medium = 0b");
  lacklustrlabs::Utils::printBinary(SOUT, medium);
  SOUT.print(" == ");
  SOUT.print(lacklustrlabs::Utils::numeric2HexString(medium));
  SOUT.println();

  uint8_t small = 0x13;
  SOUT.print("small  = 0b");
  lacklustrlabs::Utils::printBinary(SOUT, small);
  SOUT.print(" == ");
  SOUT.print(lacklustrlabs::Utils::numeric2HexString(small));
  SOUT.println();
  
  SOUT.println();
}
