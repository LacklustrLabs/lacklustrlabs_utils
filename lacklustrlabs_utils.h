#pragma once

#if defined(ARDUINO) && ARDUINO >=100
#include <Arduino.h>
#else
#include <WProgram.h>
#endif


namespace lacklustrlabs {

struct Utils {

  /**
   */
  template< typename T >
  static String numeric2HexString( const T &t ){
    uint8_t *ptr = (uint8_t*) &t;
    static const char hexb[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
    String rv("0x");
    for( size_t count = sizeof(T); count; count-- ) {
      rv += hexb[((*(ptr+count-1))>>4)&0x0f];
      rv += hexb[(*(ptr+count-1))&0x0f];
    }
    return rv;
  }

  /**
   * a Stream.print(,BIN) function that does not hide preceding '0'
   * TODO:This only works for uint and int data
   */
  template< typename T >
  static void printBinary(Stream& stream, const T val, uint16_t fromBit=0, uint16_t upToBit=sizeof(T)*8) {
    for (int16_t i=upToBit-1; i>=fromBit; i--) {
      stream.print((val>>i)&1);
    }
  }

  /**
   * a print as binary string function that does not hide preceding '0'
   * TODO:This only works for uint and int data
   */
  template< typename T >
  static String numeric2BinString(const T val, uint16_t fromBit=0, uint16_t upToBit=sizeof(T)*8) {
    String rv;

    for (int16_t i=upToBit-1; i>=fromBit; i--) {
      rv += ((val>>i)&1)?'1':'0';
    }

    return rv;
  }
};

} // namespace lacklustrlabs

